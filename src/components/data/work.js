export const WorkData=[
    {
        company:"Pikkal and Co.",
        designation:"React Developer",
        type:"Internship",
        dateJoin:"Nov 2020",
        dateEnd:"Jan 2021",
        companyLogo:require('../../assets/company-logo/pikkal.png').default,
        description:"I was responsible for developing the UI components.",
    },
    {
        company:"Saleassist.ai",
        designation:"React Developer",
        type:"Internship",
        dateJoin:"Mar 2021",
        dateEnd:"Apr 2021",
        companyLogo:require('../../assets/company-logo/saleassist.jpeg').default,
        description:"I was working as a feature integration developer.",
    },
    {
        company:"Runners Planet",
        designation:"React Developer",
        type:"Internship",
        dateJoin:"Apr 2021",
        dateEnd:"June 2021",
        companyLogo:require('../../assets/company-logo/runnersplanet.png').default,
        description:"I was responsible for implementing business logic.",
    },
]