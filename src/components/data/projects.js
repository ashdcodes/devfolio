export const ProjectData=[
    {
        id:"1",
        title:"Video Chat App",
        releaseinfo:"Coming Soon",
        about:"This is a simple, yet useful video chat application.It is designed to be light and simple,without comprosing anything on features.More features are coming,stay tuned !",
        tags:["Reactjs","Nodejs","SocketIO","MaterialUI","AWS"],
        demourl:"www.example.com",
        github:"https://github.com",
        demoimage:require('../../assets/project-image/VideoCall.png').default,
    },
    {
        id:"2",
        title:"Music Player App",
        releaseinfo:"Coming Soon",
        about:"This is a cool music player designed for music enthusiasts ! ",
        tags:["ReactNative","Android","iOS","Mobile","Firebase"],
        demourl:"www.example.com",
        github:"https://github.com",
        demoimage:require('../../assets/project-image/Music.png').default,
    },
    
]